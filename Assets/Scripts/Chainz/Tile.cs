using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Chainz
{
    public class Tile : MonoBehaviour, IPointerClickHandler
    {
        #region INIT

        private RawImage _strokeRawImage;
        private RawImage _rightConnectionRawImage;
        private RawImage _leftConnectionRawImage;
        private RawImage _bottomConnectionRawImage;
        private RawImage _topConnectionRawImage;
        private RawImage _rightConnectionStrokeRawImage;
        private RawImage _leftConnectionStrokeRawImage;
        private RawImage _bottomConnectionStrokeRawImage;
        private RawImage _topConnectionStrokeRawImage;
        private RawImage _backgroundRawImage;
        private RawImage _symbolRawImage;
        private RawImage _blockTileRawImage;
        
        private static List<Tile> _allTiles = new List<Tile>();
        
        private void Awake()
        {
            
            _allTiles.Add(this);
            GetComponent<RectTransform>().sizeDelta = new Vector2(Board.CellSize, Board.CellSize);
            
            foreach (var rawImage in GetComponentsInChildren<RawImage>())
                {
                    switch (rawImage.name)
                    {
                        case "Stroke":
                            _strokeRawImage = rawImage;
                            break;
                        case "RightConnectionStroke":
                            _rightConnectionStrokeRawImage = rawImage;
                            break;
                        case "LeftConnectionStroke":
                            _leftConnectionStrokeRawImage = rawImage;
                            break;
                        case "TopConnectionStroke":
                            _topConnectionStrokeRawImage = rawImage;
                            break;
                        case "BottomConnectionStroke":
                            _bottomConnectionStrokeRawImage = rawImage;
                            break;
                        case "RightConnection":
                            _rightConnectionRawImage = rawImage;
                            break;
                        case "LeftConnection":
                            _leftConnectionRawImage = rawImage;
                            break;
                        case "TopConnection":
                            _topConnectionRawImage = rawImage;
                            break;
                        case "BottomConnection":
                            _bottomConnectionRawImage = rawImage;
                            break;
                        case "Background":
                            _backgroundRawImage = rawImage;
                            break;
                        case "Symbol":
                            _symbolRawImage = rawImage;
                            break;
                        case "BlockTile":
                            _blockTileRawImage = rawImage;
                            break;
                    }
                    rawImage.gameObject.SetActive(rawImage.name == "Background" || rawImage.name == "Symbol");
                }
        }

        private void OnDestroy()
        {
            _allTiles.Remove(this);
        }

        #endregion

        #region SYMBOL

        private Symbol _symbol;
        
        public void SetSymbol(Symbol symbol)
        {
            if(symbol == null) return;
            _symbol = symbol;

            _symbolRawImage.texture = _symbol.iconTexture;
            _symbolRawImage.color = _symbol.iconColor;
            _backgroundRawImage.color = _symbol.backgroundColor;
            _leftConnectionRawImage.color = _symbol.backgroundColor;
            _rightConnectionRawImage.color = _symbol.backgroundColor;
            _topConnectionRawImage.color = _symbol.backgroundColor;
            _bottomConnectionRawImage.color = _symbol.backgroundColor;
            
            _blockTileRawImage.gameObject.SetActive(false);
        }

        #endregion

        #region BLOCK

        [SerializeField] private List<Texture> blockTextures;

        private int _blockLives = 3;

        private bool _isBlock = false;
        
        public void SetBlock()
        {
            _symbolRawImage.gameObject.transform.parent.gameObject.SetActive(false);
            _blockTileRawImage.texture = blockTextures[_blockLives - 1];
            _blockTileRawImage.gameObject.SetActive(true);
            _isBlock = true;
        }
            
        #endregion
        
        #region CELLING

        public Cell ParentCell { get; set; }
        
        #endregion

        #region FALLING
        
        private const float FallTimePerCell = 0.1f;
        public void TryFalling()
        {
            if (ParentCell == null) return;
            if (ParentCell.BottomNeighbour == null) return;
            if (ParentCell.BottomNeighbour.ChildTile !=null) return;
            
            StartCoroutine(FallCoroutineMethode());
        }

        public void BornFalling()
        {
            StartCoroutine(FallCoroutineMethode(true));
        }

        private IEnumerator FallCoroutineMethode(bool isBorn=false)
        {
            
            if(!isBorn)
            {
                var previousCell = ParentCell;
                ParentCell = ParentCell.BottomNeighbour;
                ParentCell.ChildTile = this;
                previousCell.ChildTile = null;
                previousCell.TryFalling();
            }
            
            var t = transform;
            var pT = ParentCell.transform;
            var fallingSpeed = Board.CellSize / FallTimePerCell;
            
            while (t.position.y > pT.position.y)
            {
                yield return null;
                t.position += Vector3.down * (Time.deltaTime * fallingSpeed);
            }
            t.position = pT.position;
            TryFalling();
            
            FindChainsForAllTiles();
        }

        

        #endregion

        #region CHAINING
        public string TypeName => _isBlock ? "Block" : _symbol.SymbolTypeName;

        public Chain ConnectedChain;

        private void FindChain()
        {
            if(_isBlock) return;
            ConnectedChain ??= new Chain(this);
            foreach (var neighbour in ParentCell.Neighbours.Where(neighbour => neighbour.ChildTile != null))
            {
                if (neighbour.ChildTile.TypeName != TypeName) continue;
                if(ConnectedChain == neighbour.ChildTile.ConnectedChain) continue;

                if (neighbour.ChildTile.ConnectedChain == null)
                {
                    neighbour.ChildTile.ConnectedChain = ConnectedChain;
                    ConnectedChain.AddTile(neighbour.ChildTile);
                }
                else
                {
                    ConnectedChain.MergeWithChain(neighbour.ChildTile.ConnectedChain);
                }
                
                neighbour.ChildTile.FindChain();
            }
            if (ConnectedChain.Length < 2) ConnectedChain = null;
        }
        
        public static void FindChainsForAllTiles()
        {
            foreach (var tile in _allTiles)
            {
                tile.ConnectedChain = null;
            }
            
            foreach (var tile in _allTiles)
            {
                tile.FindChain();
            }
            UpdateViewForAllTiles();
        }

        private Dictionary<Index.IndexType, GameObject[]> _connectionsDictionary = null;

        private  GameObject[] GetConnections(Index.IndexType indexType)
        {

            _connectionsDictionary ??= new Dictionary<Index.IndexType, GameObject[]>
            {
                {
                    Index.IndexType.Top,
                    new[] {_topConnectionRawImage.gameObject, _topConnectionStrokeRawImage.gameObject}
                },
                {
                    Index.IndexType.Bottom,
                    new[] {_bottomConnectionRawImage.gameObject, _bottomConnectionStrokeRawImage.gameObject}
                },
                {
                    Index.IndexType.Left,
                    new[] {_leftConnectionRawImage.gameObject, _leftConnectionStrokeRawImage.gameObject}
                },
                {
                    Index.IndexType.Right,
                    new[] {_rightConnectionRawImage.gameObject, _rightConnectionStrokeRawImage.gameObject}
                }
            };

            return _connectionsDictionary[indexType];

        }
        
        private static void UpdateViewForAllTiles()
        {   
            foreach (var tile in _allTiles)
            {
                if (tile._isBlock) continue;
                
                tile._strokeRawImage.gameObject.SetActive(tile.ConnectedChain != null);
                tile._symbolRawImage.color = tile.ConnectedChain != null ? Color.white : tile._symbol.iconColor;
                //tile._symbolRawImage.color = tile._symbol.iconColor;

                foreach (var indexType in Index.AllIndexTypes())
                {
                    var connections = tile.GetConnections(indexType);
                    var neighbourCell = tile.ParentCell.GetNeighbour(indexType);
                    connections[0].SetActive(
                        tile.ConnectedChain != null &&
                        neighbourCell != null &&
                        tile.ConnectedChain.Contains(neighbourCell.ChildTile));
                    connections[1].SetActive(connections[0].activeSelf);
                }
            }
        }

        #endregion
        
        #region CLICK

        public void OnPointerClick(PointerEventData eventData)
        {
            if (ConnectedChain == null) return;
            
            foreach (var tile in ConnectedChain.ContainedTiles)
            {
                tile.StartCoroutine(tile.ClickCoroutineMethode());
            }
            
        }

        private IEnumerator ClickCoroutineMethode()
        {
            var t = transform;
            var scale = 1f;
            //Handheld.Vibrate();
            Vibration.Vibrate(50);
            while (scale > 0)
            {
                t.localScale = Vector3.one * scale;
                yield return null;
                scale -= Time.deltaTime * 7f;
            }

            ParentCell.ChildTile = null;
            ParentCell.TryFalling();
            
            Destroy(this);
        }
        
        #endregion
    }
}
