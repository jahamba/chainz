using System.Collections.Generic;
using UnityEngine;

namespace Chainz
{
    [CreateAssetMenu(menuName = "Chainz/Symbol")]
    public class Symbol : ScriptableObject
    {
        public Texture iconTexture;
        public Color backgroundColor;
        public Color iconColor;

        public string SymbolTypeName => iconTexture.name;

    }
}
