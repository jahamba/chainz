
using System.Collections.Generic;
using UnityEngine;

namespace Chainz
{
    public class Chain
    {
        private readonly List<Tile> _tiles;

        public IEnumerable<Tile> ContainedTiles => _tiles;
        
        public string TypeName { get; private set; }
        public int Length => _tiles.Count;
        
        public Chain(Tile initialTile)
        {
            _tiles = new List<Tile> {initialTile};
            TypeName = initialTile.TypeName;
        }

        public bool Contains(Tile tile) => _tiles.Contains(tile);

        public void AddTile(Tile tile)
        {
            if(_tiles.Contains(tile)) return;
            _tiles.Add(tile);
        }

        public void MergeWithChain(Chain chain)
        {
            _tiles.AddRange(chain._tiles);
            foreach (var tile in chain._tiles)
            {
                tile.ConnectedChain = this;
            }
        }

       
        

    }
}
