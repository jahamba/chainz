using System.Collections.Generic;

namespace Chainz
{
    public class Index
    {
        public enum IndexType
        {
            Top,
            Bottom,
            Left,
            Right
        }

        private static IEnumerable<IndexType> _allIndexTypes = null;

        public static IEnumerable<IndexType> AllIndexTypes()
        {
            return _allIndexTypes ??= new[]
            {
                IndexType.Top, IndexType.Bottom, IndexType.Left, IndexType.Right
            };
        }
        
        public Index(int nx, int ny)
        {
            X = nx;
            Y = ny;
        }

        public Index Reversed() => new Index(-1 * X, -1 * Y);
    
        public IndexType GetIndexType()
        {
            if (X == 0)
                return Y > 0 ? IndexType.Top : IndexType.Bottom;
            return X > 0 ? IndexType.Right : IndexType.Left;
        }
        
        public int X { get; }
        public int Y { get; }
    }

    public static class NeighbourIndexes
    {
        public static Index Top { get; } = new Index(0, 1);
        public static Index Bottom { get; } = new Index(0, -1);
        public static Index Left { get; } = new Index(-1, 0);
        public static Index Right { get; } = new Index(1, 0);

        public static List<Index> LeftAndBottom { get; } = new List<Index>
        {
            Left, Bottom
        };
        
        public static List<Index> TopAndBottom { get; } = new List<Index>
        {
            Top, Bottom
        };
    }
}