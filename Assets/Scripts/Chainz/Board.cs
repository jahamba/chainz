using System.Collections.Generic;
using UnityEngine;

namespace Chainz
{
    public class Board : MonoBehaviour
    {
        #region SINGLETON

        private static Board _instance;

        private Board()
        {
            _instance = this;
        }

        #endregion
        
        #region PARAMETERS

        [SerializeField] private GameSettings gameSettings;
        private int WidthCount => gameSettings.widthCount;
        private int HeightCount => gameSettings.heightCount;

        public static List<Symbol> Symbols => _instance.gameSettings.symbols;
        
        public static float CellSize { get; private set; }
        
        
        
        [Header("Prefabs")] [SerializeField] private GameObject cellPrefab;

        [Header("Internal links")] 
        [SerializeField] private Transform cellsTransform;
        [SerializeField] private Transform tilesTransform;
        public static Transform TilesTransform => _instance.tilesTransform;
        
        #endregion

        #region INIT
        
        private void Awake()
        {
            CreateCells();
        }

        private void CreateCells()
        {
            var rect = GetComponent<RectTransform>().rect;
            CellSize = Mathf.Min(rect.width / (WidthCount + 1), rect.height / (HeightCount + 1));
            CellSize = Mathf.Round(CellSize);

            var sizeDelta = new Vector2(CellSize, CellSize);

            var x0 = Mathf.Round((rect.width - CellSize * WidthCount) / 2);
            var y0 = Mathf.Round((rect.height - CellSize * HeightCount) / 2);
            
            var cells = new Cell[WidthCount, HeightCount];

            tilesTransform.GetComponent<RectTransform>().sizeDelta = new Vector2(
                rect.width, y0 + CellSize * HeightCount); 
            
            for (var y = 0; y < HeightCount; y++)
            {
                for (var x = 0; x < WidthCount; x++)
                {
                    var cell = Instantiate(cellPrefab, cellsTransform);
                    cell.GetComponent<RectTransform>().sizeDelta = sizeDelta;

                    cell.transform.localPosition = new Vector2(
                        x0 + (x + .5f) * CellSize,
                        y0 + (y + .5f) * CellSize);
                    cells[x, y] = cell.GetComponent<Cell>();

                    foreach (var index in NeighbourIndexes.LeftAndBottom)
                    {
                        var nx = x + index.X;
                        var ny = y + index.Y;

                        if (nx < 0 || nx >= WidthCount || ny < 0 || ny >= HeightCount) continue;
                        Cell.ConnectAsNeighbours(cells[x, y], cells[nx, ny], index);
                    }
                }
            }
            Cell.InvokeBornEvent();
        }
        
        #endregion

    }
}
