using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Chainz
{
    public class Cell : MonoBehaviour
    {
        #region PREFERENCES

        [SerializeField] private GameObject tilePrefab;

        #endregion

        #region INIT

        private void Awake()
        {
            BornEvent.AddListener(BornMethode);
        }

        public static void InvokeBornEvent() => BornEvent.Invoke();

        private static readonly UnityEvent BornEvent = new UnityEvent();

        private void BornMethode()
        {
            StartCoroutine(BornCoroutineMethode());
        }

        private IEnumerator BornCoroutineMethode()
        {
            if (TopNeighbour != null) yield break;
            for (var i = 0; i < 1; i++)
            {
                yield return new WaitForSeconds(0.3f);
            }

            TryFalling();
        }

        #endregion

        #region NEIGHBOURS

        public readonly List<Cell> Neighbours = new List<Cell>();

        private Cell TopNeighbour { get; set; }
        public Cell BottomNeighbour { get; private set; }
        public Cell LeftNeighbour { get; private set; }
        private Cell RightNeighbour { get; set; }

        public Cell GetNeighbour(Index.IndexType indexType)
            => indexType switch
            {
                Index.IndexType.Top => TopNeighbour,
                Index.IndexType.Bottom => BottomNeighbour,
                Index.IndexType.Left => LeftNeighbour,
                _ => RightNeighbour
            };
    

    public static void ConnectAsNeighbours(Cell neighbour1, Cell neighbour2, Index index)
        {
            if (neighbour1 == null) return;
            if (neighbour2 == null) return;

            neighbour1.AddNeighbour(neighbour2, index);
            neighbour2.AddNeighbour(neighbour1, index.Reversed());
        }

        private void AddNeighbour(Cell neighbour, Index index)
        {
            if (Neighbours.Contains(neighbour)) return;
            Neighbours.Add(neighbour);
            switch (index.GetIndexType())
            {
                case Index.IndexType.Top:
                    TopNeighbour = neighbour;
                    break;
                case Index.IndexType.Bottom:
                    BottomNeighbour = neighbour;
                    break;
                case Index.IndexType.Left:
                    LeftNeighbour = neighbour;
                    break;
                case Index.IndexType.Right:
                    RightNeighbour = neighbour;
                    break;
            }
        }

        #endregion

        #region TILE

        public Tile ChildTile { get; set; }

        public void TryFalling()
        {
            if (ChildTile != null)
            {
                ChildTile.TryFalling();
                return;
            }

            if (TopNeighbour != null)
            {
                TopNeighbour.TryFalling();
                return;
            }

            BornCell();
        }


        private static float _blockProbability = 0f;
        
        private void BornCell()
        {
            ChildTile = Instantiate(tilePrefab, Board.TilesTransform).GetComponent<Tile>();
            ChildTile.ParentCell = this;
            ChildTile.transform.position =
                transform.position + (Vector3.up * GetComponent<RectTransform>().sizeDelta.x);

            if (Random.Range(0, 1f) < _blockProbability)
            {
                ChildTile.SetBlock();
            }
            else
            {
                _blockProbability = _blockProbability + Mathf.Pow((1 - _blockProbability) * 0.3f, 5);
                ChildTile.SetSymbol(Board.Symbols[Random.Range(0, Board.Symbols.Count)]);
            }
            
            ChildTile.BornFalling();
        }

        #endregion

    }
}
