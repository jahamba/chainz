using System.Collections.Generic;
using UnityEngine;

namespace Chainz
{
    [CreateAssetMenu(menuName = "Chainz/Game Settings")]
    public class GameSettings : ScriptableObject
    {
        public int widthCount;
        public int heightCount;

        public List<Symbol> symbols;
    }
}
